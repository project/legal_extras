<?php

/**
 * Include file
 */

/**
 * Helper function to check if user accepted the Legal terms of use
 * Note: This version check the acceptance including language version.
 */
function legal_extras_user_has_accepted($uid = NULL) {
  if (!empty($uid)) {
    $user = is_object($uid) ? $uid : user_load($uid);
  }
  else {
    global $user;
  }
  $accepted = FALSE;
  $legal = legal_extras_get_accept($user->uid);
  // If no version has been accepted yet,
  // get version with current language revision.
  if (empty($legal['version'])) {
    global $language;
    // Get version/revision of last accepted language.
    $conditions = legal_get_conditions($language->language);
    if (empty($conditions['conditions']) || !($legal['accepted'] > 0)) { // No conditions set yet or Legal not accepted.
      $accepted = legal_extras_user_has_accepted_simple($user->uid);
    }
  } else {
    // Get version/revision of last accepted language.
    $conditions = legal_get_conditions($legal['language']);
    if (empty($conditions['conditions']) || !($legal['accepted'] > 0)) { // No conditions set yet or Legal not accepted.
      $accepted = legal_extras_user_has_accepted_simple($user->uid);
    } else {
      // Check latest version of T&C has been accepted.
      $accepted = legal_version_check($user->uid, $conditions['version'], $conditions['revision'], $legal);
    }
  }
  return (bool)$accepted;
}

/** 
 * Helper function to check if user accepted the Legal terms of use
 * Note: This is simplified version with no checking version involved.
 */
function legal_extras_user_has_accepted_simple($uid = NULL) {
  if (!empty($uid)) {
    $user = user_load($uid);
  }
  else { 
    global $user;
  }
  $legal = legal_extras_get_accept($user->uid);
  return $legal['accepted'] > 0;
}   

