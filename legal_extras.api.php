<?php

/*
 * @file
 *   Legal Extra API examples
 */

/*
 * Implementation of drupal_alter callback
 *
 * This function populate the real name of the user.
 */
function foo_legal_extras_username_alter($username) {
  global $user;
  $username = $user->firstname ? ($user->firstname . ' ' . $user->surname) : $user->name;
}

/*
 * Implementation of drupal_alter callback
 *
 * This function controls if user is excepted from displaying the Terms of Use.
 */
function foo_legal_extras_force_terms_of_use_alter($show_tou) {

  // NOTE stristr() returns matched string, if matches were found
  // Ignore checking Terms of Use for non-browsers
  $agent = getenv('HTTP_USER_AGENT');
  $is_browser = !empty($agent) && stristr($agent, 'mozilla') != FALSE;
  if ( !$is_browser ) {
    $show_tou = FALSE;
    return;
  }

  // Ignore redirect on admin pages
  if (arg(0) == 'admin') {
    $show_tou = FALSE;
    return;
  }

}

/*
 * Implementation of drupal_alter callback
 *
 * This function controls if user is excepted from displaying Terms of Use.
 */
function foo_legal_extras_options_alter($options) {
  $options[1] = t('I accept the Terms & Conditions of Use') .
    '<br>' . t('Therefore I will have access to the forum.');
  $options[0] = t('I do not accept the Terms & Conditions of Use and therefore I will not have access to the forum.');
}

