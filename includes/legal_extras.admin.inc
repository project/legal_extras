<?php

/*
 * @file
 *   Include file for form pages
 */

/*
 * Menu callback for settings page
 */
function legal_extras_terms_of_use_extras(&$form_state) {
  $conditions = legal_get_conditions();
  $form['legal_extras_not_accept_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate feature where user can reject accepting the Terms of Use.'),
    '#default_value' => variable_get('legal_extras_not_accept_enabled', TRUE), 
  );
  $form['legal_extras_single_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('When non-acceptance feature is activated, show only single checkbox, instead of two.'),
    '#default_value' => variable_get('legal_extras_single_checkbox', TRUE), 
    '#disabled' => !variable_get('legal_extras_not_accept_enabled', TRUE),
  );
  $form['legal_extras_no_default_option'] = array(
    '#type' => 'checkbox',
    '#title' => t('When non-acceptance feature is activated, do not select the default option on Terms of Use page.'),
    '#default_value' => variable_get('legal_extras_no_default_option', TRUE),
    '#disabled' => !variable_get('legal_extras_not_accept_enabled', TRUE),
    );
  $form['legal_extras_display_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('When non-acceptance feature is activated, display confirming message for the user when saving Terms of Use in the user account profile.'),
    '#default_value' => variable_get('legal_extras_display_message', TRUE),
    '#disabled' => !variable_get('legal_extras_not_accept_enabled', TRUE),
    );
  $form['legal_extras_account_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Terms of Use in separate tab in user account profile.'),
    '#default_value' => variable_get('legal_extras_account_tab', TRUE), 
  );
  $form['legal_extras_display_version'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Acceptance date, Version and Revision number on Terms of Use pages.'),
    '#default_value' => variable_get('legal_extras_display_version', TRUE),
  );
  $form['legal_extras_redirect_from_legal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect user from Legal page to Terms of Use available on his account (works only for non-admin).'),
    '#default_value' => variable_get('legal_extras_redirect_from_legal', TRUE),
  );
  $form['legal_extras_show_change_summary'] = array(
    '#type' => 'checkbox',
    '#title' => t('When user already accepted previous Terms of Use, next time display summary of changes above the conditions.'),
    '#default_value' => variable_get('legal_extras_show_change_summary', TRUE),
  );
  $form['legal_extras_force_terms_of_use'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force people to accept new Terms of Use while they are logged in.'),
    '#default_value' => variable_get('legal_extras_force_terms_of_use', TRUE),
  );
  $form['legal_extras_auto_accept_revisions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not force user to re-accept Terms of Use on revision changes.'),
    '#default_value' => variable_get('legal_extras_auto_accept_revisions', TRUE),
  );
  $form['#submit'][] = 'legal_extras_terms_of_use_extras_submit';
  return system_settings_form($form);
}

/*
 * Implementation of submit Menu callback for settings page
 */
function legal_extras_terms_of_use_extras_submit(&$form_state) {
  if ($form_state['legal_extras_account_tab']['#default_value'] <> $form_state['legal_extras_account_tab']['#value']) {
    // If tab feature has been activated, rebuild the menu
    variable_set('legal_extras_account_tab', $form_state['legal_extras_account_tab']['#value']);
    menu_rebuild();
    cache_clear_all();
  }
}

/*
 * Menu callback
 */
function legal_extras_terms_of_use_form(&$form_state) {

  global $user;
  $account = user_load(arg(1));

  // Get last accepted version for this account.
  $legal_account = legal_extras_get_accept($account->uid);

  // If no version has been accepted yet,
  // get version with current language revision.
  if (empty($legal_account['version'])) {
    $conditions = legal_get_conditions($language->language);
    // No conditions set yet.
    if (empty($conditions['conditions'])) return;
    }
    else {
    // Get version/revision last accepted language.

      $conditions = legal_get_conditions($legal_account['language']);
      // No conditions set yet.
      if (empty($conditions['conditions'])) return;

      // Check latest version of T&C has been accepted.
      module_load_include('inc', 'legal_extras');
      $accepted = legal_extras_user_has_accepted($account->uid);

      // Enable language switching if version
      // accepted and revision up to date.
      if ($accepted && $legal_account['language'] != $language->language) {
        $conditions = legal_get_conditions($language->language);
      }
    }

  $form_fields = legal_display_fields($conditions);

  if ($accepted === TRUE) {
    $form_fields['legal']['legal_accept']['#value'] = 1;
    if (!empty($conditions['extras'])) {
      foreach ($conditions['extras'] as $key => $label) {
        if (!empty($label)) {
          $form_fields['legal'][$key]['#value'] = 1;
        }
      }
    }
  }

 // Disable checkbox if:
 //  - user is not account owner;
 //  - latest T&C has already been accepted.
  if ($account->uid != $user->uid || $accepted  == TRUE) {
    $form_fields['legal']['legal_accept']['#attributes'] = array('disabled' => 'disabled');
    if (!empty($conditions['extras'])) {
      reset($conditions['extras']);
      foreach ($conditions['extras'] as $key => $label) {
        if (!empty($label)) {
          $form_fields['legal'][$key]['#attributes'] = array('disabled' => 'disabled');
        }
      }
    }
  }

  // Not required if user is not account owner.
  if ($account->uid != $user->uid) {
    $form_fields['legal']['legal_accept']['#required'] = FALSE;
    if (!empty($conditions['extras'])) {
      reset($conditions['extras']);
      foreach ($conditions['extras'] as $key => $label) {
        if (!empty($label)) {
          $form_fields['legal'][$key]['#required'] = FALSE;
        }
      }
    }
  }

  // Enable account owner to accept.
  if ($account->uid == $user->uid && $accepted != TRUE) {
    $form_fields['legal']['legal_accept']['#default_value'] = isset($edit['legal_accept']) ? $edit['legal_accept'] : '';
    $form_fields['legal']['legal_accept']['#required'] = TRUE;

    if (!empty($conditions['extras'])) {
      reset($conditions['extras']);

      foreach ($conditions['extras'] as $key => $label) {
        if (!empty($label)) {
          $form_fields['legal'][$key]['#default_value'] = isset($edit[$key]) ? $edit[$key] : '';
          $form_fields['legal'][$key]['#required'] = TRUE;
        }
      }
    }
  }

  $form_fields['#uid'] = $account->uid;

  // if (!(bool)$legal_account['accepted']) { }
  $form_fields['legal']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
    '#weight' => 100,
    '#submit' => array('legal_extras_login_submit'),
  );

  $form = theme('legal_display', $form_fields);

  return $form;
}

